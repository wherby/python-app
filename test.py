import os,sys

import webapp
import unittest
import tempfile


class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        self.app = webapp.app.test_client()
        pass

    def tearDown(self):
        pass

    def test_empty_db(self):
        rv = self.app.get('/test')
        #print(rv.data)
        assert 'hello new web:test' in rv.data

if __name__ == '__main__':
    unittest.main()